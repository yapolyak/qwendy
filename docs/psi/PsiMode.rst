:gitlab_url: https://gitlab.com/yapolyak/qwendy

psi.PsiMode
==============

.. autoclass:: qwendy.psi.PsiMode
    :members:
    :undoc-members:
    :show-inheritance:


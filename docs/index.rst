:gitlab_url: https://gitlab.com/yapolyak/qwendy

Welcome to QWenDy's documentation!
==================================

**QWenDy** (Quantum Wavepacket electronic-nuclear Dynamics) is a Python implementation (*being currently under initial
development*) of a direct multidimensional molecular quantum dynamics method based on Gaussian Multiconfiguration
Time-Dependent Hartree (G-MCTDH) method, using Gaussian basis functions, optionally multilayer representation of the
wavefunction and machine-learned potential energy surfaces.

The goal of this implementation is three-fold: *scientific*
- as it will be a fresh and easy-to-extend implementation of an important method that pushes the current limits of
multidimensional quantum dynamics but is originally written in Fortran and is therefore somewhat cumbersome to develop
and interface to external programs, *educational* - as it will allow those who are new to the field to understand the
underlying algorithms and "play" with it's objects and methods, and finally *software-engineering* - as the method is
computationally very demanding and it is our intention to push to the limits of high-performance Python capabilities
and see if it can compete with Fortran or C/C++ implementations on HPC.

Theory
------

G-MCTDH is based on the MCTDH quantum dynamics method, being one of the most efficient approaches of solving
time-dependent Schrödinger equation for molecules. Starting with a definition of a wavefunction as a sum of products of
(vibrational) single-particle functions (conventionally defined on a grid):

.. math::
    \Psi({\bf q},t) = \sum_{J}A_J(t)\Phi_J({\bf q},t) = \sum_{j_1=1}^{n^{(1)}} \sum_{j_2=1}^{n^{(2)}} \cdots \sum_{j_f=1}^{n^{(f)}}A_{j_1,j_2,...j_f}(t) \prod_{\kappa=1}^{f}\chi_{j_{\kappa}}^{(\kappa)}({\bf q}_{\kappa}, t)

and by applying the Dirac-Frenkel variational principle, one obtains equations of motion (EOMs) for both the
coefficients :math:`A_{j_1,j_2,...j_f}(t)` and single-particle functions :math:`\chi_{j_{\kappa}}^{(\kappa)}({\bf q}_{\kappa}, t)`.

In G-MCTDH one-particle functions are replaced by parametrised Gaussian wavepackets, each of which may span one or more
nuclear degrees of freedom (DOF):

.. math::
    \Psi({\bf q},t) =\sum_{J}A_J G_J := \sum_{J}A_J\prod_{\kappa=1}^{f}g_{j_{\kappa}}^{(\kappa)}({\bf q}_{\kappa},t)

with :math:`g_{j_{\kappa}}^{(\kappa)}({\bf q}_{\kappa},t) = \mathrm{exp}\left[{\bf q^\intercal}_{j_{\kappa}} \boldsymbol{\varsigma}_{j_{\kappa}} {\bf q}_{j_{\kappa}} + \boldsymbol{\xi}_{j_{\kappa}} {\bf q}_{j_{\kappa}} + \eta_{j_{\kappa}}\right]`,
where :math:`\boldsymbol{\varsigma}_{j_{\kappa}}` is a 'width' (covariance) matrix, :math:`\boldsymbol{\xi}_{j_{\kappa}}`
is given by position and momentum vectors :math:`{\bf q}_{j_{\kappa}} + i{\bf p}_{j_{\kappa}}` and :math:`\eta_{j_{\kappa}}`
is complex phase.

One may want to add flexibility to such a formulation for a wavefunction by expressing it in terms of two layers, with
the first one consisting of products of arbitrary (orthonormalised) functions, which are in turn expressed as a linear
combination of GWPs:

.. math::
    \Psi({\bf q},t) =\sum_{J}A_J \Phi_J = \sum_{J}A_J\prod_{\kappa=1}^{f}\chi_{j_{\kappa}}^{(\kappa)} = \sum_{J}A_J\prod_{\kappa=1}^{f} \left(\sum_{L}B_{j_{\kappa},L}^{(\kappa)} G_L^{(\kappa)}\right)

.. math::
    = \sum_{J}A_J(t)\prod_{\kappa=1}^{f} \left( \sum_{L}B_{j_{\kappa},L}^{(\kappa)}(t) \prod_{\mu=1}^{f^{(\kappa)}} g_{l_{\mu}}^{(\kappa,\mu)}({\bf q}_{\kappa,\mu}, t) \right)

This can be further extended to three and more layers if needed. One of the goals of the current project is to implement
an arbitrarily-deep multilayer formulation of a wavefunction and its efficient propagation.

*...more theory to follow...*

------

References to the background theory
-----------------------------------
1. | I. Burghardt, H.-D. Meyer, and L. S. Cederbaum, “Approaches to the approximate treatment of complex molecular systems by the multiconfiguration time-dependent Hartree method” J. Chem. Phys. 111, 2927–2939 (1999), https://doi.org/10.1063/1.479574
2. | I. Burghardt, M. Nest, and G. A. Worth, “Multiconfigurational system-bath dynamics using Gaussian wave packets: Energy relaxation and decoherence induced by a finite-dimensional bath” J. Chem. Phys. 119(11), 5364–5378 (2003), https://doi.org/10.1063/1.1599275
3. | I. Burghardt, K. Giri, and G. A. Worth, “Multimode quantum dynamics using Gaussian wavepackets: The Gaussian-based multiconfiguration time-dependent Hartree (G-MCTDH) method applied to the absorption spectrum of pyrazine” J. Chem. Phys. 129(17), 174104 (2008), https://doi.org/10.1063/1.2996349
4. | W. Koch,  M. Bonfanti, P. Eisenbrandt, A. Nandi, B. Fu, J. Bowman, D. Tannor, and  I. Burghardt, “Two-layer Gaussian-based MCTDH study of the S\ :sub:`1`\ ← S\ :sub:`0`\  vibronic absorption spectrum of formaldehyde using multiplicative neural network potentials” J. Chem. Phys. 151, 064121 (2019), https://doi.org/10.1063/1.5113579

Table of Contents
=================
.. toctree::
   :maxdepth: 4

   psi/index.rst
   hamiltonian/index.rst
   fast_math.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

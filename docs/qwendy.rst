qwendy package
==============

Submodules
----------

qwendy.fast\_math module
------------------------

.. automodule:: qwendy.fast_math
   :members:
   :undoc-members:
   :show-inheritance:

qwendy.hamiltonian module
-------------------------

.. automodule:: qwendy.hamiltonian
   :members:
   :undoc-members:
   :show-inheritance:

qwendy.psi module
-----------------

.. automodule:: qwendy.psi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: qwendy
   :members:
   :undoc-members:
   :show-inheritance:

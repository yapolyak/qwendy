:gitlab_url: https://gitlab.com/yapolyak/qwendy

hamiltonian.Hterm
==============

.. autoclass:: qwendy.hamiltonian.Hterm
    :members:
    :undoc-members:
    :show-inheritance:


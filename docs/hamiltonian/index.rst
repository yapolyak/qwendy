:gitlab_url: https://gitlab.com/yapolyak/qwendy

qwendy.hamiltonian
==========

Classes
-------
.. toctree::
   :maxdepth: 2

   Hamiltonian.rst
   Hterm.rst
   PES.rst

Methods
-------
:todo:

FROM python:3.7
RUN pip install numpy
RUN pip install numba
RUN pip install pytest
RUN pip install sphinx
RUN pip install sphinx_rtd_theme

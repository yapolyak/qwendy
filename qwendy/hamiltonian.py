import numpy as np

"""
Contains classes related to molecular nuclear Hamiltonian

Input for Hamiltonian:
A dictionary, generally containing kinetic energy (KE) and potential energy (PE) entries. Each of the entries is a list
of dictionaries, one per term, which contain entries such as 'dof', either a string or a list, bearing the same name(s)
as in the wavefunction definition, 'term', containing the term formula symbol(s) (q, q^2, q^3, dq, dq^2 etc.) and
'coeff', containing a value of a term coefficient (defaulting to 1 if not present).
KE entry can instead contain a dictionary instead of a list, which would contain an entry 'type' (e.g. 'Rectilinear'),
which would then be interpreted by the parser into a corresponding formula, as well as other optional entries.
PE entry can also alternatively contain a dictionary, that would contain an entry 'type', specifying the type of the
potential function (e.g. 'LHA' or 'GPR' for direct calculation or 'HO' for harmonic oscillator) and other entries that
may be specific for a given type, e.g. 'parameters', which value would be a list of smaller dictionaries with parameters
per term, or 'filename' containing the name of the file that would contain the fitted surface function.
Examples:
>>> hamiltonian = {'KE': {'type': 'rectilinear'}, 'PE': {'type': 'HO', 'parameters': [{'dof': 'v1', 'freq': 0.001},
                                                                                      {'dof': 'v2', 'freq': 0.002}]}}
or
>>> hamiltonian = {'KE': [{'dof': 'v1', 'term': 'dq^2', 'coeff': 0.5}, {'dof': ['v2, v3'], 'term': ['dq', 'dq']}],
                   'PE': [{'dof': 'v1', 'term': 'q^2', 'coeff': 1.0}, {'dof': ['v2', 'v3'], 'term': ['q', 'q']}]}
"""


class Hamiltonian:
    """
    Main Hamiltonian class. It's instance will contain a list of Hamiltonian terms, being instances of Hterm class.
    """
    def __init__(self, name=None):
        self.name = name
        self.pes = None
        self.terms = []
        self.type = {}
        self.dofs = []
        self.ndof = 0
        self.coeffs = []
        self.freqs = []

    @classmethod
    def parse_input_dict(cls, inp_dict=None):
        """
        Parses input dictionary containing Hamiltonian parameters and instantiates Hamiltonian and Hterms objects

        :param inp_dict: Input dictionary
        :return: Hamiltonian class instance
        """
        if inp_dict is None:
            raise SyntaxError('A null-pointer has been passed to Hamiltonian parser')
        elif not inp_dict:
            raise SyntaxError('Only a non-empty Hamiltonian input can be parsed')
        h = cls('main')
        for key, value in inp_dict.items():
            if key == 'KE' or key == 'PE':
                if isinstance(value, list):
                    h.type[key] = 'manual'
                    for term in value:
                        h.terms.append(Hterm(key))
                        h.terms[-1].read_term(**term)
                elif isinstance(value, dict):
                    h.type[key] = 'model'
                    for key_, value_ in value.items():
                        if key_ == 'type':
                            h.type[key] = value_
                        elif key_ == 'dofs':
                            if isinstance(value_, list):
                                h.dofs = value_
                                h.ndof = len(value_)
                            else:
                                raise SyntaxError(f'Key \'dofs\' should have a list of strings as value')
                        elif key_ == 'freqs':
                            if isinstance(value_, list):
                                h.freqs = value_
                            else:
                                raise SyntaxError(f'Key \'freqs\' should have a list of strings as value')
                        elif key_ == 'coeffs':
                            if isinstance(value_, list):
                                h.coeffs = value_
                            else:
                                raise SyntaxError(f'Key \'coeffs\' should have a list of strings as value')
                        elif key_ == 'parameters':
                            if isinstance(value_, list):
                                for term in value_:
                                    h.terms.append(Hterm(key))
                                    h.terms[-1].read_term(**term)
                            else:
                                raise SyntaxError(f'Key \'parameters\' should have a list of dicts as value')
                        else:
                            raise SyntaxError(f'Model Hamiltonian argument {key_} not implemented')
                else:
                    raise SyntaxError(f'Currently only lists or dictionaries can be parsed as KE or PE arguments')
            else:
                raise SyntaxError(f'Currently only \'KE \' or \'PE\' entries are allowed in Hamiltonian specification')
        if not h.terms and h.dofs:
            for idof, hdof in enumerate(h.dofs):
                h.terms.append(Hterm('KE'))
                h.terms[idof].dof.append(hdof)
                h.terms[idof].ndof = 1
                if h.type['KE'] == 'rectilinear':
                    h.terms[idof].form = "dq^2"
            for idof, hdof in enumerate(h.dofs):
                h.terms.append(Hterm('PE'))
                h.terms[h.ndof+idof].dof.append(hdof)
                h.terms[h.ndof+idof].ndof = 1
                if h.type['PE'] == 'HO':
                    h.terms[h.ndof+idof].form = "q^2"
                if h.coeffs:
                    h.terms[h.ndof+idof].coeff = h.coeffs[idof]
                if h.freqs:
                    h.terms[h.ndof+idof].freq = h.freqs[idof]
        return h


class Hterm:
    """
    Class which instances represent Hamiltonian terms, which can be products of various degrees of freedom.
    """
    def __init__(self, typ=None):
        self.type = typ
        self.dof = []
        self.ndof = 0
        self.form = []
        self.coeff = 1
        self.freq = 0

    def read_term(self, **kwargs):
        for key, value in kwargs.items():
            if key == 'dof':
                if isinstance(value, list):
                    self.dof.extend(value)
                elif isinstance(value, str):
                    self.dof.extend([value])
                else:
                    raise SyntaxError(f'Key \'dof\' should be followed by a string or a list of strings')
                self.ndof = len(self.dof)
            elif key == 'term':
                if isinstance(value, list):
                    self.form.extend(value)
                elif isinstance(value, str):
                    self.form.extend([value])
                else:
                    raise SyntaxError(f'Key \'term\' should be followed by a string or a list of strings')
            elif key == 'coeff':
                self.coeff = value
            elif key == 'frequency':
                self.freq = value
            else:
                raise SyntaxError(f'Hamiltonian argument {key} not implemented')
        if not self.dof:
            raise SyntaxError(f'Degrees of freedom (\'dof\') entry should be present for each Hamiltonian term')
        elif self.form:
            assert len(self.dof) == len(self.form)


class PES(Hamiltonian):
    """
    This class will contain members and methods necessary to evaluate and/or manage potential energy surfaces
    """
    pass

import numpy as np
from numba import guvectorize, njit

"""
Contains Numba jit and generalised ufuncs methods to deal with expensive algebra
"""


@guvectorize('c16[:, :, :], c16[:, :, :], c16[:, :, :, :]', '(m, n, n), (m, n, n) -> (m, m, n, n)')
def add_cmat_stack_outer(a, b, c):
    """
    Performs outer summation of two stacks of complex matrices element-wise, with the left matrix being conjugated
    """
    for i in range(a.shape[0]):
        for j in range(a.shape[0]):
            c[i, j] = np.add(np.conj(a[i]), b[j])


@guvectorize('c16[:, :], c16[:, :], c16[:, :, :]', '(m, n), (m, n) -> (m, m, n)')
def add_cvec_stack_outer(a, b, c):
    """
    Performs outer summation of two stacks of complex vectors element-wise, with the left matrix being conjugated
    """
    for i in range(a.shape[0]):
        for j in range(a.shape[0]):
            c[i, j] = np.add(np.conj(a[i]), b[j])


@guvectorize('c16, c16[:], c16[:]', '(), (m) -> (m)')
def mom1_vec(gf_a, gf_b, mom1):  # Is this needed?
    """
    Calculate a 1D-array of first-order Gaussian moments

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param mom1: First-order Gaussian moments - vector
    :return: First-order Gaussian moments - vector
    """
    for i in range(gf_b.shape[0]):
        mom1[i] = -0.5 * gf_a * gf_b[i]


@guvectorize('c16, c16[:], c16[:, :], c16[:, :]', '(), (m), (m, n) -> (m, n)')
def mom2_mat(gf_a, gf_b, z_inv, mom2):  # Is this really needed?
    """
    Calculate a 2D-array of second-order Gaussian moments

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param z_inv: Inverse of Zeta ("width") matrix
    :param mom2: Second-order Gaussian moments - matrix
    :return: Second-order Gaussian moments - matrix
    """
    for i in range(z_inv.shape[0]):
        for j in range(z_inv.shape[0]):
            mom2[i, j] = 0.25 * gf_a * (gf_b[i] * gf_b[j] - 2.0 * z_inv[i, j])


# @guvectorize('c16, c16[:], int64, c16[:]', '(), (m), () -> ()')
@njit
def moment1(gf_a, gf_b, i):
    """
    Calculate a first-order Gaussian moment

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param i: moment index
    :return: First-order Gaussian moment - scalar
    """
    return -0.5 * gf_a * gf_b[i]


# @guvectorize('c16, c16[:], c16[:, :], u4, u4, c16', '(), (m), (m, n), (), () -> ()')
@njit
def moment2(gf_a, gf_b, z_inv, i, j):
    """
    Calculate a second-order Gaussian moment

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param z_inv: Inverse of Zeta ("width") matrix
    :param i: moment index
    :param j: moment index
    :return: Second-order Gaussian moment - scalar
    """
    return 0.25*gf_a*(gf_b[i]*gf_b[j] - 2.0*z_inv[i, j])


#@guvectorize('c16, c16[:], c16[:, :], u4, u4, u4, c16', '(), (m), (m, n), (), (), () -> ()')
@njit
def moment3(gf_a, gf_b, z_inv, i, j, k):
    """
    Calculate a third-order Gaussian moment

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param z_inv: Inverse of Zeta ("width") matrix
    :param i: moment index
    :param j: moment index
    :param k: moment index
    :return: Third-order Gaussian moment - scalar
    """
    return -0.125*gf_a*(gf_b[i]*gf_b[j]*gf_b[k] -
                        2.0*(z_inv[i, j]*gf_b[k] + gf_b[i]*z_inv[j, k] + gf_b[j]*z_inv[i, k]))


#@guvectorize('c16, c16[:], c16[:, :], u4, u4, u4, u4, c16', '(), (m), (m, n), (), (), (), () -> ()')
@njit
def moment4(gf_a, gf_b, z_inv, i, j, k, l):
    """
    Calculate a fourth-order Gaussian moment

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param z_inv: Inverse of Zeta ("width") matrix
    :param i: moment index
    :param j: moment index
    :param k: moment index
    :param l: moment index
    :return: Fourth-order Gaussian moment - scalar
    """
    return 0.0625*gf_a * (gf_b[i]*gf_b[j]*gf_b[k]*gf_b[l]
                          - 2.0*(gf_b[i]*gf_b[j]*z_inv[k, l] + gf_b[i]*gf_b[k]*z_inv[j, l]
                                 + gf_b[i]*gf_b[l]*z_inv[j, k] + gf_b[j]*gf_b[k]*z_inv[i, l]
                                 + gf_b[j]*gf_b[l]*z_inv[i, k] + gf_b[k]*gf_b[l]*z_inv[i, j])
                          + 4.0*(z_inv[i, j]*z_inv[k, l] + z_inv[i, k]*z_inv[j, l] + z_inv[i, l]*z_inv[j, k]))


@njit
def unravel_mat_coord(dim, idx):
    """
    Analogue to numpy.unravel_index() for a square matrix

    :param dim: matrix dimension
    :param idx: flat index
    :return: matrix element coordinates
    """
    mat_ = np.arange(dim * dim).reshape(dim, dim)  # model matrix filled with indices from 0 to ndof*ndof
    mat_upp = np.triu(mat_)
    mat_flat = np.flatnonzero(mat_upp)
    idx = mat_flat[idx]
    i = idx//dim
    j = idx - dim*(idx//dim)
    return i, j


@guvectorize('c16, c16[:], c16[:, :], c16[:, :], c16[:, :]', '(), (m), (m, n), (o, p) -> (o, p)')
def construct_smat(gf_a, gf_b, z_inv, dum, smat):
    """
    Construct overlap and partial derivative (with respect to parameters) overlap matrices (smat array)

    :param gf_a: Generating function, 1st term - scalar
    :param gf_b: Generating function, 2nd term - vector
    :param z_inv: Inverse of Zeta ("width") matrix
    :param dum: Dummy array of dimensions nparam+1 x nparam+1 - needed to make gufunc work
    :param smat: Array of overlap matrices: local dimensions nparam+1 x nparam+1
    :return: Array of overlap matrices
    """
    nparam = dum.shape[0]-1
    ndof = gf_b.shape[0]
    # First calculate a triangular matrix - does it matter if row or column-major?
    smat[0, 0] = gf_a  # S^{00} element
    for i in range(1, ndof+1):  # dGWP wrt Xi-vector parameters
        smat[0, i] = moment1(gf_a, gf_b, i-1)
        for j in range(1, i+1):
            smat[j, i] = moment2(gf_a, gf_b, z_inv, j-1, i-1)
    if nparam > ndof:  # 'SG' and 'TG'
        for i in range(ndof+1, 2*ndof+1):  # dGWP wrt Zeta-matrix diagonal parameters
            i_ = i-ndof-1
            smat[0, i] = moment2(gf_a, gf_b, z_inv, i_, i_)
            for j in range(1, ndof+1):
                smat[j, i] = moment3(gf_a, gf_b, z_inv, j-1, i_, i_)
            for j in range(ndof+1, i+1):
                j_ = j-ndof-1
                smat[j, i] = moment4(gf_a, gf_b, z_inv, j_, j_, i_, i_)
    if nparam > 2*ndof:  # 'TG' only
        for i in range(2*ndof+1, nparam+1):
            i_, ii_ = unravel_mat_coord(ndof, i-2*ndof-1)
            smat[0, i] = 2*moment2(gf_a, gf_b, z_inv, i_, ii_)
            for j in range(1, ndof+1):
                smat[j, i] = 2*moment3(gf_a, gf_b, z_inv, j-1, i_, ii_)
            for j in range(ndof+1, 2*ndof+1):
                j_ = j-ndof-1
                smat[j, i] = 2*moment4(gf_a, gf_b, z_inv, j_, j_, i_, ii_)
            for j in range(2*ndof+1, i+1):
                j_, jj_ = unravel_mat_coord(ndof, j-2*ndof-1)
                smat[j, i] = 4*moment4(gf_a, gf_b, z_inv, j_, jj_, i_, ii_)
    # Fill (symmetric) lower-triangle
    for i in range(1, nparam+1):
        for j in range(i+1):
            smat[i, j] = smat[j, i]

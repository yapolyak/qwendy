"""
QWenDY
=======

Python implementation of direct multilayer multidimensional molecular quantum dynamics method (G-MCTDH),
using Gaussian basis functions and machine learned potential energy surfaces

"""

from .psi import parse_psi, update_upwards, init_coeff
from .hamiltonian import Hamiltonian

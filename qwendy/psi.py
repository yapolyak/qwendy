import numpy as np
from . import fast_math

"""
Contains the classes responsible for the wavefunction multilayer tree structure as well as an input parser and other 
utility methods

Present the multi-layer wavefunction (Psi) as a nested dictionary. Each entry represents a number of (Chi^mu_n)
nth-level , mu-mode functions. The last level is represented by parameterised basis functions (Gaussians),
which in turn contain a dictionary of initial parameters per dimension (width, position-> to be changed by the program,
momentum and frequency) i.e. 
:math:`Psi = Sum(A_i П_mu(Chi^mu_i:[Sum(B_j П_mu(Chi^mu_j: ... {Sum(D_l) П_mu(G^mu_l)}))]))`
"""


class PsiMode:
    """
    A class allowing for creation and manipulation of a tree-like structure of a wavefunction
    """
    def __init__(self, name=None, layer=None):
        self.name = name
        self.layer = layer
        self.modes = []
        self.nmod = 0
        self.neighbours = []
        self.parent = None
        self.num = 0
        self.coeff = None
        self.ncoeff = 0
        self.dof = []
        self.ndof = 0

    def next(self, mode=0):
        return self.modes[mode]

    @property
    def left(self):
        return next(self.parent.modes[i-1] for i, obj in enumerate(self.parent.modes) if obj == self)

    @property
    def right(self):
        return next(self.parent.modes[i+1] for i, obj in enumerate(self.parent.modes) if obj == self)

    def add(self, name=None, layer=None):
        if name is None:
            name = 'L'+str(self.layer)+'_'+str(len(self.modes))
        if 'GWP' in name:
            new_mode = GWP(name, layer)
        else:
            new_mode = Phi(name, layer)
        self.modes.append(new_mode)
        self.nmod += 1
        new_mode.parent = self
        for imode, mode in enumerate(self.modes):
            mode.neighbours = self.modes[:imode]+self.modes[imode+1:]
        return new_mode


class Phi(PsiMode):
    """
    Class describing a general SPF (single-particle function) of an arbitrary level
    """
    def __init__(self, name=None, layer=None):
        super().__init__(name, layer)

    def density_gwp_single_layer(self):
        """
        Calculate reduced density matrices for the GWP modes.
        To be used for single-layer Psi only! (to be moved to GWP class?)

        :return: Reduced (single hole) density matrices for each GWP mode
        """
        assert self.num == 1
        avec = np.zeros_like(self.coeff)
        segstride = []
        seglength = []
        segstride.append(self.modes[0].num)
        seglength.append(1)
        for i, mode in enumerate(self.modes[:-1], 1):
            seglength.append(seglength[i-1]*mode.num)
        for i, mode in enumerate(self.modes[1:], 1):
            segstride.append(segstride[i-1]*mode.num)
        for igwp, gwp in enumerate(self.modes):  # to be re-written using Numba gufuncs?
            rest = (self.ncoeff / gwp.num).astype(int)  # length of the coefficient matrix to be shaped
            avec = avec.reshape(gwp.num, rest)
            for i in range(gwp.num):  # re-shuffle coefficient vector / matrix appropriate to the current mode
                nstride = int(rest/seglength[igwp])  # must be exactly divisible
                for j in range(nstride):
                    avec[i, j*seglength[igwp]:j*seglength[igwp]+seglength[igwp]] = \
                        self.coeff[j*segstride[igwp]+i*seglength[igwp]:j*segstride[igwp]+i*seglength[igwp]+seglength[igwp]]
            m_rest = [mode for mode in self.modes if mode is not gwp]  # All modes but the current
            outer = m_rest[0].smat[:, :, 0, 0]
            for i, mode in enumerate(m_rest[1:], 1):  # Calculate outer product matrix of GWP products
                outer = np.kron(mode.smat[:, :, 0, 0], outer)  # We scale (i-1)'th mode with i'th, etc.
            gwp.rhomat = np.conj(avec) @ outer @ np.transpose(avec)  # The main operation

    def density(self):
        """
        Calculate general-type (for any mode of any layer) reduced density matrix
        """
        pass


class GWP(PsiMode):
    """
    A class containing variables and methods attributed to each GWP object in wavefunction
    """
    def __init__(self, name=None, layer=None):
        super().__init__(name, layer)
        self.depth = 0
        self.type = 'FG'
        self.nparam = 0
        self.a = None      #: width todo: should it be stored? Or just as zeta?
        self.q = None      #: position vector
        self.p = None      #: momentum vector
        self.qp = None     #: q + ip vector
        self.omega = None  #: mode frequencies
        self.zeta = None   #: 1/[2*(width^2)]
        self.xi = None     #: -2Aq + ip vector
        self.eta = None    #: phase
        self.gf_a = None   #: generating function for GWP moments, first term
        self.gf_b = None   #: generating function for GWP moments, second term
        self.z_inv = None  #: inverse of the Zeta (width) matrix
        self.mom0 = None   #: Zero-order moment (matrix)
        self.mom1 = None   #: First-order moment (rank-3 tensor)
        self.mom2 = None   #: Second-order moment (rank-4 tensor)
        self.smat = None   #: Overlap matrices (rank-4 tensor)
        self.rhomat = None  #: Reduced density matrix

    def read_param(self, **kwargs):
        for key, value in kwargs.items():
            if key == 'num':
                self.num = value
            elif key == 'dof':
                self.dof = value
                self.ndof = len(value)
            elif key == 'type':
                self.type = value
            elif key == 'width':
                # Should there be a possibility to input full matrix?
                self.a = (np.zeros((self.num, len(value), len(value)), np.complex128))
                # Set diagonal values of a ngwp x ndof x ndof width array to the values of the input list
                width = [0.25 * (1/(w**2)) for w in value]  # Need to check the factor of 1/4
                np.einsum('ijj->ij', self.a)[...] = width
            elif key == 'pos':
                self.q = (np.zeros((self.num, len(value)), np.float64))
                self.q = self.q + np.asarray(value)
            elif key == 'mom':
                self.p = (np.zeros((self.num, len(value)), np.float64))
                self.p = self.p + np.asarray(value)
            elif key == 'freq':
                self.omega = (np.zeros((self.num, len(value)), np.float64))
                self.omega = self.omega + np.asarray(value)

    def init_param(self):
        """
        - Set zeta, xi and eta from width, q and p from input.
        - Initialise GWP distribution? -> TBD
        """
        if self.type == 'SG':  # "Separable" GWP
            self.nparam = self.ndof * 2  # Xi vector + Zeta matrix diagonal
        elif self.type == 'TG':  # "Thawed" GWP
            self.nparam = 0.5 * self.ndof * (self.ndof+3)  # Xi vector + upper-triangular Zeta matrix
        else:  # default: 'FG' - "Frozen" GWP
            self.nparam = self.ndof  # Only Xi vector
        self.zeta = -self.a
        self.xi = -2 * np.einsum('ijk,ik->ij', self.zeta, self.q) + 1j * self.p  # Xi = -2Zeta*q + ip
        self.qp = self.q + 1j*self.p
        self.eta = np.einsum('ki,kij,kj->k', self.xi, self.zeta, self.xi) - 1j * np.einsum('ij,ij->i', self.q, self.p)
        # To keep GWPs normalised - use numba @vectorise? :
        self.eta = self.eta - np.sum(0.5 * np.log(np.sqrt(np.pi / (2 * np.real(np.linalg.eigvals(self.a))))), axis=1)  # Check

    def moments(self):
        """
        Evaluates Gaussian moments generating function terms and moments up to second order
        """
        # Should moments not be stored and instead only their generating functions?
        # 1) Take product (conjugate) of two GWPs
        zeta_ovl = np.zeros((self.num, self.num, self.ndof, self.ndof), np.complex128)
        zeta_ovl = fast_math.add_cmat_stack_outer(self.zeta, self.zeta, zeta_ovl)
        xi_ovl = np.zeros((self.num, self.num, self.ndof), np.complex128)
        xi_ovl = fast_math.add_cvec_stack_outer(self.xi, self.xi, xi_ovl)
        eta_ovl = np.add.outer(np.conj(self.eta), self.eta)
        # 2) Invert "width" matrix and evaluate moments generating function terms
        self.z_inv = np.linalg.inv(zeta_ovl)  # is regularisation needed? -> then need diagonalisation or SVD
        self.gf_b = np.einsum('klij,klj->kli', self.z_inv, xi_ovl)
        self.gf_a = np.einsum('kli,kli->kl', xi_ovl, self.gf_b)
        self.gf_a = np.sqrt(np.pi / np.linalg.det(zeta_ovl)) * np.exp(eta_ovl - 0.25 * self.gf_a)
        # 3) Calculate moments 0-2 - is this necessary?
        self.mom0 = self.gf_a.copy()
        self.mom1 = np.zeros_like(self.gf_b)
        self.mom1 = fast_math.mom1_vec(self.gf_a, self.gf_b, self.mom1)
        self.mom2 = np.zeros_like(zeta_ovl)
        self.mom2 = fast_math.mom2_mat(self.gf_a, self.gf_b, self.z_inv, self.mom2)

    def overlaps(self):
        """
        Evaluates Gaussian overlap and partial derivative (with respect to parameters) overlap matrices
        """
        pdim = int(self.nparam+1)
        self.smat = np.zeros((self.num, self.num, pdim, pdim), np.complex128)
        dum = np.zeros_like(self.smat)  # Not ideal but is needed to make Numba happy
        self.smat = fast_math.construct_smat(self.gf_a, self.gf_b, self.z_inv, dum, self.smat)  # Does extra work...
        # "Hermitize" overlap matrices
        i, j = np.triu_indices(self.num, 1)
        self.smat[i, j] = np.conj(self.smat[j, i])
        # Set diagonal elements in the GWP space real-only
        diag = np.diag_indices(self.num)
        idiag = self.smat[diag].imag
        self.smat[diag] = self.smat[diag] - idiag*1j

    def keo_me_rl(self):
        r"""
        Kinetic Energy Operator Matrix Elements calculation.
        Assume dealing with rectilinear coordinates, i.e. KEO has the form:

        :math:`\sum_{i} \frac{\partial^2}{\partial^2 q_i}`
        """
        self.mass = np.zeros(self.ndof, np.float64)
        self.mass = self.mass - 0.5  # Temporary - should be -0.5*M where M is reduced mass of the DOF
        self.x0 = np.zeros(self.num, np.complex128)
        self.x0 = (np.einsum('ijj->ij', self.zeta)[...]) @ self.mass \
                  + np.einsum('ij,ij->i', self.xi, self.mass * self.xi)
        self.x1 = np.zeros((self.num, self.ndof), np.complex128)
        self.x1 = 4.0 * np.einsum('ijk,ik->ij', self.mass * self.zeta, self.xi)
        self.x2 = np.zeros((self.num, self.ndof, self.ndof), np.complex128)
        self.x2 = 4.0 * self.zeta @ (self.mass * self.zeta)
        # contract
        self.ke = np.zeros((self.num, self.num), np.complex128)
        self.ke = self.x0 * self.mom0 + np.einsum('ijk,jk->ij', self.mom1, self.x1) \
                  + np.einsum('ijkl,jkl->ij', self.mom2, self.x2)

    def renormalise(self):
        """
        Reevaluates GWPs phase (Eta) to keep them normalised
        """
        pass

    def get_param(self):
        pass


def parse_psi(l, layer=0, mode=None, gwp=None, root=None):
    """
    Recursively parses an input dictionary defining the wavefunction multilayer structure
    and creates objects for every mode as part of a (wavefunction) tree node

    :param l: input dictionary (the only parameter to be used when called outside of :func:`psi.parse_psi`)
    :param layer: wavefunction tree layer id
    :param mode: current mode (i.e. node)
    :param gwp: global list of GWP objects (leafs of the last nodes in a branch)
    :param root: the root mode (node)
    :return: a tuple containing list of GWPs and the root mode (node)
    """
    if layer == 0:
        mode = PsiMode('L0', layer)
        root = mode
        gwp = []
    if isinstance(l, dict):
        if not l:
            raise SyntaxError("Input dict empty")
        if 'GWP' in mode.name:
            mode.read_param(**l)
        else:
            for key in l.keys():
                if key == 'num':
                    mode.num = l[key]
                elif key == 'modes':
                    parse_psi(l[key], layer, mode, gwp, root)
    elif isinstance(l, list):
        if not l:
            raise SyntaxError("Input list empty")
        layer += 1
        for i, item in enumerate(l):
            if isinstance(item, dict) and 'width' in item:
                mode = mode.add('GWP'+str(i), layer)
                gwp.append(mode)
            else:
                mode = mode.add('L'+str(layer)+'_'+str(i), layer)
            parse_psi(item, layer, mode, gwp, root)
            mode = mode.parent
    return gwp, root


def update_upwards(gwps):
    """
    Updates all the modes in the tree with the corresponding (sub-)list and number of the lowest-level GWP DOFs;
    Sets depth of each GWP branch of the tree

    :param gwps: list of objects of the GWP class
    """
    for gwp in gwps:
        mode = gwp
        gwp.depth = 0
        while mode.name != 'L0':
            mode.parent.dof.extend(gwp.dof)
            mode.parent.ndof += gwp.ndof
            mode = mode.parent
            gwp.depth += 1
    return


def init_coeff(gwps):
    """
    Initialize time-dependent coefficients at each layer of the wavefunction expansion

    :param gwps: list of objects of the GWP class
    """
    for gwp in gwps:  # A bit of redundant work, but done once, so should be OK
        mode = gwp
        for i in range(gwp.depth):
            mode = mode.parent
            if 'L0' in mode.name:
                mode.ncoeff = np.prod([mode.modes[j].num for j in range(len(mode.modes))])
            else:
                mode.ncoeff = mode.num * np.prod([mode.modes[j].num for j in range(len(mode.modes))])
            if mode.coeff is None:
                mode.coeff = np.zeros(mode.ncoeff, np.complex128)
    for gwp in gwps:
        gwp.parent.coeff[0] = 1.0
    # Need to look into orthonormalizaton of the other layers initial coefficients


def level_of(mode):
    """
    Get all the modes from the level of a given mode, that have the same parent with the given mode

    :param mode: any object of PsiModes (any mode of any level)
    :return: list of modes of the current level that have the same parent with the input mode
    """
    mode_list = [mode]
    for m in mode.neighbours:
        mode_list.append(m)
    return mode_list

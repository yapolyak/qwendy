import numpy as np
import qwendy as qw

"""
Initial program that showcases what is currently implemented
"""

# 3-layer composition of a 12-D problem may look like this
psi = [{'num': 3, 'modes': [{'num': 1, 'modes': [{'num': 2, 'type': 'TG', 'dof': ['v1', 'v2'], 'width': [0.707, 0.707],
                                                  'pos': [0.5, 0.1], 'mom': [0.1, 0.1], 'freq': [1000.0, 1000.0]},
                                                 {'num': 5, 'type': 'FG', 'dof': ['v3'], 'width': [0.707],
                                                  'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]}]},
                            {'num': 1, 'modes': [{'num': 5, 'type': 'FG', 'dof': ['v4'], 'width': [0.707],
                                                  'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]},
                                                 {'num': 3, 'type': 'FG', 'dof': ['v5', 'v6'], 'width': [0.707, 0.707],
                                                  'pos': [0.0, 0.0], 'mom': [0.0, 0.0], 'freq': [1000.0, 1000.0]}]}]},
       {'num': 3, 'modes': [{'num': 1, 'modes': [{'num': 3, 'type': 'FG', 'dof': ['v7', 'v8'], 'width': [0.707, 0.707],
                                                  'pos': [0.0, 0.0], 'mom': [0.0, 0.0], 'freq': [1000.0, 1000.0]},
                                                 {'num': 5, 'type': 'FG', 'dof': ['v9', 'v10'], 'width': [0.707, 0.707],
                                                  'pos': [0.0, 0.0], 'mom': [0.0, 0.0], 'freq': [1000.0, 1000.0]}]},
                            {'num': 1, 'modes': [{'num': 4, 'type': 'FG', 'dof': ['v11'], 'width': [0.707],
                                                  'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]},
                                                 {'num': 4, 'type': 'FG', 'dof': ['v12'], 'width': [0.707],
                                                  'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]}]}]}]

hamiltonian = {'KE': {'type': 'rectilinear'},
               'PE': {'type': 'HO', 'dofs': ['v1', 'v2', 'v3', 'v4', 'v5', 'v6',
                                             'v7', 'v8', 'v9', 'v10', 'v11', 'v12'],
                                    'freqs': [0.001, 0.001, 0.001, 0.001, 0.001, 0.001,
                                              0.001, 0.001, 0.001, 0.001, 0.001, 0.001]}}

gwps, root = qw.parse_psi(psi)
qw.update_upwards(gwps)
qw.init_coeff(gwps)
H = qw.Hamiltonian.parse_input_dict(hamiltonian)

# Temporary test printouts
for term in H.terms:
    print(f'H-term of type {term.type}: {term.dof}')

for gwp in gwps:
    print(f'{gwp.name} ngwp: {gwp.num} depth: {gwp.depth} -> '
          f'{gwp.parent.name} nphi: {gwp.parent.num} nmod: {gwp.parent.nmod} ncoeff: {gwp.parent.ncoeff} '
          f'-> {gwp.parent.parent.name} nphi: {gwp.parent.parent.num} nmod: {gwp.parent.parent.nmod} ncoeff: {gwp.parent.parent.ncoeff}'
          f'-> {gwp.parent.parent.parent.name} nphi: {gwp.parent.parent.parent.num} nmod: {gwp.parent.parent.parent.nmod}')
    gwp.init_param()
    # print(f'Zeta: {gwp.zeta},\n Xi: {gwp.xi},\n Eta: {gwp.eta}')
    gwp.moments()
    gwp.overlaps()
    # Will need a general wrapper function that will decide which methods to call to calculate MEs
    gwp.keo_me_rl()
    print(f'KEO ME: {gwp.ke}')

for gwp in gwps:
    if gwp is gwp.parent.modes[0]:
        gwp.parent.density_gwp_single_layer()  # Should it be rather a gwp class method?
    print(f'{gwp.name} reduced density matrix: \n {gwp.rhomat}')




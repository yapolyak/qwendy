import pytest
from qwendy.hamiltonian import Hamiltonian


class TestParseHamiltonian:
    def test_empty(self):
        hamiltonian = {}
        with pytest.raises(SyntaxError):
            h = Hamiltonian.parse_input_dict(None)
        with pytest.raises(SyntaxError):
            h = Hamiltonian.parse_input_dict(hamiltonian)

    def test_manual(self):
        hamiltonian = {
            'KE': [{'dof': 'v1', 'term': 'dq^2', 'coeff': 0.5}, {'dof': 1, 'term': ['dq', 'dq']}],
            'PE': [{'dof': 'v1', 'term': 'q^2', 'coeff': 1.0}, {'dof': ['v2', 'v3'], 'term': ['q', 'q']}]}
        with pytest.raises(SyntaxError):
            h = Hamiltonian.parse_input_dict(hamiltonian)
        hamiltonian = {
            'KE': [{'dof': 'v1', 'term': 'dq^2', 'coeff': 0.5}, {'dof': ['v2', 'v3'], 'term': 1}],
            'PE': [{'dof': 'v1', 'term': 'q^2', 'coeff': 1.0}, {'dof': ['v2', 'v3'], 'term': ['q', 'q']}]}
        with pytest.raises(SyntaxError):
            h = Hamiltonian.parse_input_dict(hamiltonian)

import numpy as np
from qwendy.fast_math import *


class TestStackOuter:
    def test_add_cmat(self):
        a = np.arange(8, dtype=np.complex128).reshape(2, 2, 2)
        b = a.copy()
        c = np.zeros((a.shape[0], a.shape[0], a.shape[1], a.shape[2]), np.complex128)
        c = add_cmat_stack_outer(a, b, c)
        assert c[0, 0, 0, 0] == 0
        assert c[0, 1, 0, 1] == 6
        assert c[1, 0, 1, 0] == 8
        assert c[1, 1, 1, 1] == 14

    def test_add_cvec(self):
        a = np.arange(8, dtype=np.complex128).reshape(2, 4)
        b = a.copy()
        c = np.zeros((a.shape[0], a.shape[0], a.shape[1]), np.complex128)
        c = add_cvec_stack_outer(a, b, c)
        assert c[0, 0, 3] == 6
        assert c[0, 1, 3] == 10
        assert c[1, 0, 3] == 10
        assert c[1, 1, 3] == 14

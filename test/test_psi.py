import pytest
from qwendy.psi import parse_psi, update_upwards


class TestParsePsi:
    def test_empty_list(self):
        psi = []
        with pytest.raises(SyntaxError):
            gwps, root = parse_psi(psi)
        psi = [{'num': 1, 'modes': []}]
        with pytest.raises(SyntaxError) as excinfo:
            gwps, root = parse_psi(psi)
        assert "list empty" in str(excinfo.value)

    def test_empty_dict(self):
        psi = [{}]
        with pytest.raises(SyntaxError):
            gwps, root = parse_psi(psi)
        psi = [{'num': 1, 'modes': [{}]}]
        with pytest.raises(SyntaxError) as excinfo:
            gwps, root = parse_psi(psi)
        assert "dict empty" in str(excinfo.value)

    def test_three_layers(self):
        psi = [{'num': 3, 'modes': [{'num': 1, 'modes': [{'num': 2, 'type': 'TG', 'dof': ['v1', 'v2'], 'width': [0.707, 0.707],
                                                          'pos': [0.5, 0.1], 'mom': [0.1, 0.1], 'freq': [1000.0, 1000.0]},
                                                         {'num': 5, 'type': 'FG', 'dof': ['v3'], 'width': [0.707],
                                                          'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]}]},
                                    {'num': 1, 'modes': [{'num': 5, 'type': 'FG', 'dof': ['v4'], 'width': [0.707],
                                                          'pos': [0.0], 'mom': [0.0], 'freq': [1000.0]},
                                                         {'num': 3, 'type': 'FG', 'dof': ['v5', 'v6'], 'width': [0.707, 0.707],
                                                          'pos': [0.0, 0.0], 'mom': [0.0, 0.0], 'freq': [1000.0, 1000.0]}]}]}]

        gwps, root = parse_psi(psi)
        update_upwards(gwps)
        assert gwps[0].ndof == 2
        assert gwps[3].num == 3
        assert gwps[2].parent.num == 1
        assert gwps[1].parent.parent.nmod == 2
        assert root.ndof == 6
